use crate::row::Row;
use crate::data::DataType;

const DATA_SEPARATOR: u8 = ':' as u8;
const COL_SEPARATOR: u8 = '|' as u8;
const ROW_SEPARATOR: u8 = '\n' as u8;

/// Metadata definition for a table. It contains
/// the column types and names.
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Metadata {
    column_types: Vec<DataType>,
    column_names: Vec<[u8; 10]>,
}

impl Metadata {
    pub fn new(column_types: Vec<DataType>,  column_names: Vec<[u8; 10]>) -> Self {
        Self {
            column_types,
            column_names
        }
    }

    /// Converts a `Metadata` to a byte vector.
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut v: Vec<u8> = Vec::new();
        for (index, col_type) in self.column_types.iter().enumerate() {
            let col_name = self.column_names.get(index).unwrap();
            match col_type {
                DataType::Long => {
                    v.push('l' as u8);
                }
                DataType::Boolean => {
                    v.push('b' as u8);
                }
                DataType::String => {
                    v.push('s' as u8);
                }
            }
            v.push(DATA_SEPARATOR);
            for c in col_name.iter() {
                v.push(c.clone());
            }
            v.push(COL_SEPARATOR);
        }
        v
    }

    /// Converts a byte vector to a `Metadata`.
    pub fn from_bytes(bytes: Vec<u8>) -> Metadata {
        let mut cts: Vec<DataType> = Vec::new();
        let mut cns: Vec<[u8; 10]> = Vec::new();
        let mut columns = bytes.split(|c| c == &(COL_SEPARATOR));
        loop {
            match columns.next() {
                Some(col) => {
                    if col.len() == 0 {
                        continue;
                    }

                    match col[0] as char {
                        'l' => {
                            cts.push(DataType::Long)
                        }
                        'b' => {
                            cts.push(DataType::Boolean)
                        }
                        's' => {
                            cts.push(DataType::String)
                        }
                        _ => {
                            eprintln!("Unknown data type: {}", col[0]);
                            panic!()
                        }
                    }
                    let mut cn = [0 as u8; 10];
                    cn.copy_from_slice(&col[2..]);
                    cns.push(cn);
                },
                None => { break }
            }
        }
        Metadata {
            column_types: cts,
            column_names: cns
        }
    }

    /// Converts a `Metadata` to a string.
    pub fn to_string(&self) -> String {
        let mut s: Vec<String> = Vec::new();
        for (index, col_type) in self.column_types.iter().enumerate() {
            let col_name = self.column_names.get(index).unwrap();
            match col_type {
                DataType::Long => {
                    s.push("long".to_string());
                }
                DataType::Boolean => {
                    s.push("boolean".to_string());
                }
                DataType::String => {
                    s.push("string".to_string());
                }
            }
            s.push(":".to_string());

            let valid: Vec<u8> = col_name.iter()
                .filter(|&c| c != &0x00)
                .cloned()
                .collect();

            s.push(String::from_utf8(valid).unwrap());
            s.push("|".to_string());
        }
        s.join("")
    }
}

/// Definition for a table within rdb. It contains
/// a vector of all of the rows. It also contains
/// a vector of the metadata to know the column order.
#[derive(Debug)]
#[derive(PartialEq)]
pub struct Table {
    metadata: Metadata,
    rows: Vec<Row>,
}

impl Table {
    pub fn new(metadata: Metadata, rows: Vec<Row>) -> Self {
        Self {
            metadata,
            rows
        }
    }

    /// Converts a `Table` to a byte vector.
    pub fn to_bytes(&self) -> Vec<u8> {
        let mut v: Vec<u8> = Vec::new();
        v.extend(self.metadata.to_bytes());
        for row in self.rows.iter() {
            v.push(ROW_SEPARATOR);
            for data in row.to_bytes() {
                v.extend(data.to_vec());
            }
        }
        v
    }

    /// Converts a byte vector to a `Table`.
    pub fn from_bytes(bytes: Vec<u8>) -> Table {
        let mut data = bytes.split(|c| c == &(ROW_SEPARATOR));
        let mut metadata_row = true;
        let mut md: Option<Metadata> = None;
        let mut rs: Vec<Row> = Vec::new();
        loop {
            match data.next() {
                Some(d) => {
                    if metadata_row {
                        md = Some(Metadata::from_bytes(d.to_vec()));
                        metadata_row = false;
                    } else {
                        let mut chunks = d.chunks(12);
                        let mut r_vec: Vec<[u8; 12]> = Vec::new();
                        loop {
                            match chunks.next() {
                                Some(chunk) => {
                                    let mut r: [u8; 12] = [0 as u8; 12];
                                    r.copy_from_slice(&chunk[0..12]);
                                    r_vec.push(r);
                                },
                                None => { break }
                            }
                        }
                        rs.push(Row::from_bytes(r_vec));
                    }
                },
                None => { break }
            }
        }

        match md {
            Some(mdo) => {
                Table {
                    metadata: mdo,
                    rows: rs
                }
            },
            None => {
                eprintln!("Metadata for Table was not parsed.");
                panic!();
            }
        }
    }

    /// Converts a `Table` to a string.
    pub fn to_string(&self) -> String {
        let mut s: Vec<String> = Vec::new();
        s.push(self.metadata.to_string());
        for row in &self.rows {
            s.push(row.to_string());
        }
        s.join("\n")
    }
}

/// Tests for Metadata and Table
#[cfg(test)]
mod tests {
    use crate::table::{COL_SEPARATOR, DATA_SEPARATOR, ROW_SEPARATOR};
    use crate::table::{Metadata, Table};
    use crate::data::{DataType, Data};
    use crate::row::Row;

    /// Ensures metadata -> byte vector works as expected.
    #[test]
    fn metadata_to_bytes() {
        let ct = vec![DataType::String, DataType::String, DataType::Boolean, DataType::Long];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_boolean".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let mut c4 = [0 as u8; 10];
        for (index, c) in "long_field".chars().enumerate() {
            c4[index] = c as u8;
        }
        cn.push(c4);
        let metadata = Metadata {
            column_types: ct,
            column_names: cn,
        };

        let bytes = metadata.to_bytes();
        assert_eq!(52, bytes.len());
        let expected_bytes = vec![
            's' as u8, DATA_SEPARATOR, 'c' as u8, 'o' as u8, 'l' as u8, 'u' as u8, 'm' as u8, 'n' as u8, '1' as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            's' as u8, DATA_SEPARATOR, 'c' as u8, '2' as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            'b' as u8, DATA_SEPARATOR, 'm' as u8, 'y' as u8, '_' as u8, 'b' as u8, 'o' as u8, 'o' as u8, 'l' as u8, 'e' as u8, 'a' as u8, 'n' as u8, COL_SEPARATOR,
            'l' as u8, DATA_SEPARATOR, 'l' as u8, 'o' as u8, 'n' as u8, 'g' as u8, '_' as u8, 'f' as u8, 'i' as u8, 'e' as u8, 'l' as u8, 'd' as u8, COL_SEPARATOR
        ];
        assert_eq!(expected_bytes, bytes);
    }

    /// Ensures byte vector -> metadata works as expected.
    #[test]
    fn metadata_from_bytes() {
        let bytes = vec![
            's' as u8, DATA_SEPARATOR, 'c' as u8, 'o' as u8, 'l' as u8, 'u' as u8, 'm' as u8, 'n' as u8, '1' as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            's' as u8, DATA_SEPARATOR, 'c' as u8, '2' as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            'b' as u8, DATA_SEPARATOR, 'm' as u8, 'y' as u8, '_' as u8, 'b' as u8, 'o' as u8, 'o' as u8, 'l' as u8, 'e' as u8, 'a' as u8, 'n' as u8, COL_SEPARATOR,
            'l' as u8, DATA_SEPARATOR, 'l' as u8, 'o' as u8, 'n' as u8, 'g' as u8, '_' as u8, 'f' as u8, 'i' as u8, 'e' as u8, 'l' as u8, 'd' as u8, COL_SEPARATOR
        ];

        let metadata = Metadata::from_bytes(bytes);
        assert_eq!(4, metadata.column_types.len());
        assert_eq!(4, metadata.column_names.len());
    }

    /// Ensures metadata -> string works as expected.
    #[test]
    fn metadata_to_string() {
        let ct = vec![DataType::String, DataType::String, DataType::Boolean, DataType::Long];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_boolean".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let mut c4 = [0 as u8; 10];
        for (index, c) in "long_field".chars().enumerate() {
            c4[index] = c as u8;
        }
        cn.push(c4);
        let metadata = Metadata {
            column_types: ct,
            column_names: cn,
        };

        let s = metadata.to_string();
        let expected_string = "string:column1|string:c2|boolean:my_boolean|long:long_field|".to_string();
        assert_eq!(expected_string, s);
    }

    /// Ensures table -> byte vector works as expected.
    #[test]
    fn table_to_bytes() {
        let bd1 = Data::new(DataType::Boolean,[0x01].to_vec());
        let ld1 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd1 = Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row1 = Row::new(vec![bd1, ld1, sd1]);
        let bd2 = Data::new(DataType::Boolean,[0x00].to_vec());
        let ld2 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39].to_vec());
        let sd2 = Data::new(DataType::String,[0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row2 = Row::new(vec![bd2, ld2, sd2]);

        let ct = vec![DataType::Boolean, DataType::Long, DataType::String];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_string".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let m = Metadata {
            column_types: ct,
            column_names: cn,
        };

        let table = Table {
            metadata: m,
            rows: vec![row1, row2],
        };

        let bytes = table.to_bytes();
        assert_eq!(113, bytes.len());
        let mut expected_bytes: Vec<u8> = Vec::new();
        expected_bytes.extend(table.metadata.to_bytes());
        expected_bytes.push(ROW_SEPARATOR);
        expected_bytes.extend(['b' as u8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00].to_vec());
        expected_bytes.extend(['l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x00, 0x00, 0x00].to_vec());
        expected_bytes.extend(['s' as u8, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        expected_bytes.push(ROW_SEPARATOR);
        expected_bytes.extend(['b' as u8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00].to_vec());
        expected_bytes.extend(['l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39, 0x00, 0x00, 0x00].to_vec());
        expected_bytes.extend(['s' as u8, 0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        assert_eq!(expected_bytes, bytes);
    }

    /// Ensures byte vector -> table works as expected.
    #[test]
    fn bytes_to_table() {
        let bytes = [
            'b' as u8, DATA_SEPARATOR, 'c' as u8, 'o' as u8, 'l' as u8, 'u' as u8, 'm' as u8, 'n' as u8, '1' as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            'l' as u8, DATA_SEPARATOR, 'c' as u8, '2' as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, 0 as u8, COL_SEPARATOR,
            's' as u8, DATA_SEPARATOR, 'm' as u8, 'y' as u8, '_' as u8, 's' as u8, 't' as u8, 'r' as u8, 'i' as u8, 'n' as u8, 'g' as u8, 0 as u8, COL_SEPARATOR,
            ROW_SEPARATOR,
            'b' as u8, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            'l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38, 0x00, 0x00, 0x00,
            's' as u8, 0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64,
            ROW_SEPARATOR,
            'b' as u8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            'l' as u8, 0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39, 0x00, 0x00, 0x00,
            's' as u8, 0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64
        ].to_vec();

        let table = Table::from_bytes(bytes);

        let ct = vec![DataType::Boolean, DataType::Long, DataType::String];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_string".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let expected_metadata = Metadata {
            column_types: ct,
            column_names: cn,
        };
        assert_eq!(expected_metadata, table.metadata);
        assert_eq!(2, table.rows.len());
        let bd1 = Data::new(DataType::Boolean,[0x01].to_vec());
        let ld1 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd1 = Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row1 = Row::new(vec![bd1, ld1, sd1]);
        assert_eq!(&row1, table.rows.get(0).unwrap());
        let bd2 = Data::new(DataType::Boolean,[0x00].to_vec());
        let ld2 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39].to_vec());
        let sd2 = Data::new(DataType::String,[0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row2 = Row::new(vec![bd2, ld2, sd2]);
        assert_eq!(&row2, table.rows.get(1).unwrap());
    }

    /// Ensures table -> string works as expected.
    #[test]
    fn table_to_string() {
        let bd1 = Data::new(DataType::Boolean,[0x01].to_vec());
        let ld1 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x38].to_vec());
        let sd1 = Data::new(DataType::String,[0x48, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x57, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row1 = Row::new(vec![bd1, ld1, sd1]);
        let bd2 = Data::new(DataType::Boolean,[0x00].to_vec());
        let ld2 = Data::new(DataType::Long,[0x31, 0x32, 0x33, 0x34, 0x35, 0x36, 0x37, 0x39].to_vec());
        let sd2 = Data::new(DataType::String,[0x68, 0x65, 0x6C, 0x6C, 0x6F, 0x20, 0x77, 0x6F, 0x72, 0x6C, 0x64].to_vec());
        let row2 = Row::new(vec![bd2, ld2, sd2]);

        let ct = vec![DataType::Boolean, DataType::Long, DataType::String];
        let mut cn: Vec<[u8; 10]> = Vec::new();

        let mut c1 = [0 as u8; 10];
        for (index, c) in "column1".chars().enumerate() {
            c1[index] = c as u8;
        }
        cn.push(c1);

        let mut c2 = [0 as u8; 10];
        for (index, c) in "c2".chars().enumerate() {
            c2[index] = c as u8;
        }
        cn.push(c2);

        let mut c3 = [0 as u8; 10];
        for (index, c) in "my_string".chars().enumerate() {
            c3[index] = c as u8;
        }
        cn.push(c3);

        let m = Metadata {
            column_types: ct,
            column_names: cn,
        };

        let table = Table {
            metadata: m,
            rows: vec![row1, row2],
        };

        let s = table.to_string();
        let expected_string = "\
                                    boolean:column1|long:c2|string:my_string|\n\
                                    true,12345678,Hello World\n\
                                    false,12345679,hello world\
                                    ".to_string();
        assert_eq!(expected_string, s);
    }
}